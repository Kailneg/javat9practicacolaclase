package main;

import java.util.NoSuchElementException;

import models.Lista;

public class Main {

	public static void main(String[] args) {

		//Creo nueva lista. A�ado a,b,c
		Lista<Character> lista = new Lista<>();
		lista.add('a');
		lista.add('b');
		lista.add('c');
		
		print(lista.getCurrent());
		print(lista.getPrev());
		print(lista.getPrev());
		print(lista.getPrev());
		print(lista.getPrev());
		
		//Borro c
		System.out.println("Borro c");
		lista.remove('c');
		print(lista.getCurrent());
		print(lista.getNext());
		print(lista.getNext());
		print(lista.getNext());
		
		System.out.println("A�ado c y d, set index a 1 (b)");
		lista.add('c');
		lista.add('d');
		lista.setIndex(1);
		lista.setIndex(1234); //Compruebo error
		print(lista.getCurrent()); //Debe mostrar b
		
		//Borro todos
		System.out.println("Borro todos");
		lista.remove('a');
		lista.remove('b');
		lista.remove('c');
		lista.remove('d');
		
		/*
		 * En estos momentos, cualquier intento de acceder a los m�todos getCurrent,
		 * getNext o getPrev, dar� como resultado un NoSuchElementException
		 */
		try {
			lista.getCurrent();
		} catch (NoSuchElementException e) {
			System.out.println("[ERROR] " + e.getMessage());
		}
		try {
			lista.getNext();
		} catch (NoSuchElementException e) {
			System.out.println("[ERROR] " + e.getMessage());
		}
		try {
			lista.getPrev();
		} catch (NoSuchElementException e) {
			System.out.println("[ERROR] " + e.getMessage());
		}
		
		//A�ado z y hago la misma operacion
		lista.add('z');
		print(lista.getCurrent());
		print(lista.getNext());
		print(lista.getPrev());
		lista.remove('z');
		
		
		//Intentar borrar un elemento que no existe da NoSuchElementException
		try {
			lista.remove('z');
		} catch (NoSuchElementException e) {
			System.out.println("[ERROR] " + e.getMessage());
		}
	}

	public static void print(Object o) {
		System.out.println("\nPrint-------------------------");
		System.out.println(o);
	}
	
	public static void printCola(Lista<?> l){
		System.out.println("\nPrintLista-------------------------");
		for (int i = 0; i < l.size(); i++) {
			System.out.println(l.getCurrent());
		}
	}
}
