package models;

import java.util.NoSuchElementException;

public class Lista <T>{
	
	//Atributos
	private Object[] elementos;
	private int index;

	public Lista(){
		elementos = new Object[0];
		index = 0;
	}
	
	/**
	 * Devuelve el tama�o de la lista.
	 * @return un entero con el tama�o de la lista.
	 */
	public int size(){ return elementos.length; }
	
	/**
	 * A�ade el elemento pasado por par�metros al final de la lista y posiciona el index a este elemento.
	 * @param elemento el elemento que se desea a�adir.
	 */
	public void add(T elemento) {
		Object[] nuevoArray = new Object[this.elementos.length+1];
		nuevoArray[elementos.length] = elemento;
		//Posiciono el index apuntando a este elemento
		index = elementos.length;
		//Relleno la lista de atr�s hacia delante
		for (int i = 0; i < elementos.length; i++)
			nuevoArray[i] = elementos[i];
		elementos = nuevoArray;
		
	}
	
	/**
	 * Elimina de la lista el elemento pasado por par�metros.
	 * @param elemento el elemtento que se desea eliminar.
	 */
	public void remove(T elemento) throws NoSuchElementException {
		int posicion = -1;
		boolean encontrado = false;
		
		//Buscando si el elemento existe
		for (int i = 0; i < elementos.length && !encontrado; i++) {
			if (elementos[i].equals(elemento)){
				posicion = i;
				encontrado = true;
			}
		}
		
		//Si existe, se redimensiona el array y crea uno nuevo sin el elemento,
		//posicionando el index al elemento anterior al borrado
		if (encontrado) {
			index = posicion-1;
			Object[] nuevoArray = new Object[elementos.length-1];
			for (int i = 0, j = i; i < elementos.length; i++, j++)
				if (i != posicion)
					nuevoArray[j] = elementos[i];
				else
					j--;
			elementos = nuevoArray;
		}
		else
			throw new NoSuchElementException("El elemento pasado por par�metros no se encuentra en la lista.");
		
	}
	
	/**
	 * Devuelve el elemento siguiente al que apunte el index. Si no existe lanza NoSuchElementException.
	 * @return el elemento siguiente al que apunta el �ndice, si existe.
	 */
	public T getNext() throws NoSuchElementException {
		T t = get(1);
		if (t == null)
			throw new NoSuchElementException("No existe elemento siguiente.");
		else
			return t;
	}
	
	/**
	 * Devuelve el elemento anterior al que apunte el index. Si no existe lanza NoSuchElementException.
	 * @return el elemento anterior al que apunta el �ndice, si existe.
	 */
	public T getPrev() throws NoSuchElementException {
		T t = get(-1);
		if (t == null)
			throw new NoSuchElementException("No existe elemento anterior.");
		else
			return t;
	}
	
	/**
	 * Devuelve el elemento actual al que apunte el index. Si no existe lanza NoSuchElementException.
	 * @return el elemento actual al que apunta el �ndice, si existe.
	 */
	public T getCurrent() throws NoSuchElementException {
		T t = get(0);
		if (t == null)
			throw new NoSuchElementException("No existe elemento actual.");
		else
			return t;
	}
	
	/**
	 * Establece el �ndice a i siempre que la lista admita el tama�o. En caso contrario devuelve -1.
	 * @param i el nuevo �ndice a establecer.
	 * @return el nuevo �ndice si ha sido posible establecer. De lo contrario -1.
	 */
	public int setIndex(int i) {
		return (i < elementos.length) ? index = i : -1;
	}
	
	/**
	 * M�todo interno, utilizado para encontrar un elemento a trav�s de los m�todos get*
	 * y establecer el �ndice en su posici�n.
	 * @param n�mero entero olo acepta -1 (getPrev), 0 (getCurrent) o 1 (getNext
	 * @return el elemento encontrado, si existe. De lo contrario devuelve null.
	 */
	private T get(int i) {
		switch (i) {
		case -1:
			if (elementos.length > 0) {
				if (index == 0){
					index = elementos.length-1;
					return (T) elementos[index];
				}
				else 
					return (T) elementos[--index];
			}
			else
				return null;
		case 0:
			return (elementos.length > 0) ? (T) elementos[index] : null;
		case 1:
			if (elementos.length > 0) {
				if (index == elementos.length-1) {
					index = 0;
					return (T) elementos[0];
				}
				else
					return (T) elementos[++index];
			}
			else 
				return null;
		default: return null;
		}
	}
}
