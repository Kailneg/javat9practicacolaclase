package tests;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Test;

import models.Lista;

public class ListTest {
	private Lista<Integer> l;

	@Test
	public void AddSuccess() {
		Setup();
		l.add(4);
		assert(l.getCurrent() == 4);
	}
	
	@Test
	public void AddFail() {
		Setup();
		l.add(4);
		assertFalse(l.getCurrent() != 4);
	}
	
	@Test
	public void GetNextSuccess() {
		Setup();
		assert(l.getNext() == 0);
	}
	
	@Test
	public void GetPrevSuccess() {
		Setup();
		assert(l.getPrev() == 2);
	}
	
	@Test
	public void GetCurrentSuccess() {
		Setup();
		assert(l.getCurrent() == 3);
	}
	
	@Test
	public void SetIndexSuccess() {
		Setup();
		assert(l.setIndex(1) == 1);
	}
	
	@Test
	public void SetIndexFail() {
		Setup();
		assert(l.setIndex(99) == -1);
	}
	
	@Test
	public void RemoveSuccess() {
		Setup();
		l.remove(3);
		assert(l.getCurrent() == 2);
		assert(l.size() == 3);
	}
	
	@Test
	public void RemoveFail() {
		Setup();
		try{
			l.remove(99);
		} catch(NoSuchElementException e){
			assert(e.getMessage().equals("El elemento pasado por parámetros no se encuentra en la lista."));
		}
	}
	
	private void Setup(){
		l = new Lista<Integer>();
		l.add(0);
		l.add(1);
		l.add(2);
		l.add(3);		
	}

}
